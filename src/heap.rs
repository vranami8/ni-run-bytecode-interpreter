use std::{collections::HashMap, io::Write, time::{SystemTime, UNIX_EPOCH}};

use crate::{
    program::{Classes, ConstantPoolIndex},
    runtime_object::RuntimeObject, string_write::StringWrite,
};

pub type HeapPointer = usize;

type ObjectTable = HashMap<HeapPointer, ObjectRef>;

const TAG_BIT_SIZE: u32 = 2;
const PTR_PAYLOAD_BIT_SIZE: u32 = usize::BITS - TAG_BIT_SIZE;

const INTEGER_TAG: usize = 1 << PTR_PAYLOAD_BIT_SIZE;
const BOOLEAN_TAG: usize = 2 << PTR_PAYLOAD_BIT_SIZE;
const NULL_TAG: usize = 3 << PTR_PAYLOAD_BIT_SIZE;

const PTR_PAYLOAD_MASK: usize = usize::MAX >> TAG_BIT_SIZE;

pub const NULL_PTR: HeapPointer = usize::MAX;

pub const PTR_SIZE: usize = HeapPointer::BITS as usize / 8;

enum ObjectRef {
    Object(HeapPointer),
    Array(HeapPointer),
}

pub struct Heap<W: Write> {
    memory: Vec<u8>,
    object_table: ObjectTable,
    next_obj_ptr: HeapPointer,
    mem_start: HeapPointer,
    free_cursor: HeapPointer,
    free_mem_end: HeapPointer,
    logging_output: Option<W>,
}

/**
 * This implementation uses an object table to store information about object types and their
 * memory pointers. It is also used to virtualize pointers, so the objects can be moved
 * inside the memory by the GC without impacting the rest of the implementation.
 */
impl<W: Write> Heap<W> {
    pub fn new(size: usize, logging_output: Option<W>) -> Self {
        let mut heap = Heap {
            memory: vec![0; size],
            object_table: ObjectTable::new(),
            next_obj_ptr: 0,
            mem_start: 0,
            free_cursor: 0,
            free_mem_end: size / 2,
            logging_output
        };

        heap.log_start();
        heap
    }

    fn get_next_obj_pointer(&mut self) -> HeapPointer {
        let ptr = self.next_obj_ptr;
        self.next_obj_ptr = self.next_obj_ptr + 1;
        ptr
    }

    fn add_object_entry(&mut self, obj_ptr: HeapPointer, object: ObjectRef) {
        self.object_table.insert(obj_ptr, object);
    }

    fn write_bytes(&mut self, bytes: &[u8]) {
        for byte in bytes {
            let cell = self.memory.get_mut(self.free_cursor).unwrap();
            *cell = *byte;
            self.free_cursor = self.free_cursor + 1;
        }
    }

    pub fn alloc_primitve(&mut self, object: &RuntimeObject) -> HeapPointer {
        object.into()
    }

    pub fn alloc_array(&mut self, size: u32, initial_value: HeapPointer) -> HeapPointer {
        let obj_ptr = self.get_next_obj_pointer();
        let mem_ptr = self.free_cursor;

        self.write_bytes(&size.to_ne_bytes());

        let initial_value_bytes = initial_value.to_ne_bytes();

        for _ in 0..size {
            self.write_bytes(&initial_value_bytes);
        }

        self.add_object_entry(obj_ptr, ObjectRef::Array(mem_ptr));

        self.log_alloc();

        obj_ptr
    }

    pub fn alloc_object(
        &mut self,
        class: ConstantPoolIndex,
        parent: HeapPointer,
        fields: Vec<HeapPointer>,
    ) -> HeapPointer {
        let obj_ptr = self.get_next_obj_pointer();
        let mem_ptr = self.free_cursor;

        self.write_bytes(&class.to_ne_bytes());
        self.write_bytes(&parent.to_ne_bytes());

        for field in fields {
            self.write_bytes(&field.to_ne_bytes());
        }

        self.add_object_entry(obj_ptr, ObjectRef::Object(mem_ptr));

        self.log_alloc();

        obj_ptr
    }

    pub fn write_ptr(&mut self, to: HeapPointer, ptr: HeapPointer) {
        let ptr_bytes = ptr.to_ne_bytes();

        for i in 0..PTR_SIZE {
            let memory_ref = self
                .memory
                .get_mut(to + i)
                .unwrap_or_else(|| panic!("Memory out of bounds read"));

            *memory_ref = ptr_bytes[i];
        }
    }

    pub fn read_ptr(&self, from: HeapPointer) -> HeapPointer {
        if from + PTR_SIZE > self.memory.len() {
            panic!("Heap out of bounds read")
        }

        let mut bytes: [u8; PTR_SIZE] = [0, 0, 0, 0, 0, 0, 0, 0];
        bytes.copy_from_slice(&self.memory[from..from + PTR_SIZE]);

        HeapPointer::from_ne_bytes(bytes)
    }

    fn read_u32(&self, ptr: HeapPointer) -> u32 {
        if ptr + 4 > self.memory.len() {
            panic!("Heap out of bounds read")
        }

        let mut bytes: [u8; 4] = [0, 0, 0, 0];
        bytes.copy_from_slice(&self.memory[ptr..ptr + 4]);

        u32::from_ne_bytes(bytes)
    }

    fn read_u16(&self, ptr: HeapPointer) -> u16 {
        if ptr + 2 > self.memory.len() {
            panic!("Heap out of bounds read")
        }

        let mut bytes: [u8; 2] = [0, 0];
        bytes.copy_from_slice(&self.memory[ptr..ptr + 2]);

        u16::from_ne_bytes(bytes)
    }

    pub fn read_object(&self, obj_ptr: HeapPointer) -> RuntimeObject {
        if obj_ptr.is_tagged() {
            return obj_ptr.read_tagged_value();
        }

        let object_ref = self
            .object_table
            .get(&obj_ptr)
            .unwrap_or_else(|| panic!("Invalid pointer {}", obj_ptr));

        match object_ref {
            ObjectRef::Array(memory_ptr) => RuntimeObject::Array {
                size: self.read_u32(*memory_ptr),
                content: memory_ptr + 4,
            },
            ObjectRef::Object(memory_ptr) => RuntimeObject::Object {
                class: self.read_u16(*memory_ptr),
                parent: self.read_ptr(memory_ptr + 2),
                content: memory_ptr + 2 + PTR_SIZE,
            },
        }
    }

    pub fn has_space(&mut self, size: usize) -> bool {
        self.free_cursor + size < self.free_mem_end
    }

    fn copy_ptrs(
        &mut self,
        ptrs_start: HeapPointer,
        ptrs_end: HeapPointer,
        classes: &Classes,
        new_object_table: &mut ObjectTable,
    ) {
        for ptr_to_ptr in (ptrs_start..ptrs_end).step_by(PTR_SIZE) {
            let ptr = self.read_ptr(ptr_to_ptr);
            self.copy_object(ptr, classes, new_object_table);
        }
    }

    fn copy_memory(&mut self, start: HeapPointer, end: HeapPointer) {
        for i in start..end {
            let byte = *self.memory.get(i).unwrap();

            let mem_ref = self.memory.get_mut(self.free_cursor).unwrap();
            *mem_ref = byte;

            self.free_cursor = self.free_cursor + 1;
        }
    }

    fn copy_object(
        &mut self,
        obj_ptr: HeapPointer,
        classes: &Classes,
        new_object_table: &mut ObjectTable,
    ) {
        if obj_ptr.is_tagged() || new_object_table.contains_key(&obj_ptr) {
            return;
        }

        match *self.object_table.get(&obj_ptr).unwrap() {
            ObjectRef::Array(mem_ptr) => {
                let size = self.read_u32(mem_ptr);
                let obj_end = mem_ptr + 4 + size as usize * PTR_SIZE;

                new_object_table
                    .insert(obj_ptr, ObjectRef::Array(self.free_cursor));

                self.copy_memory(mem_ptr, obj_end);

                self.copy_ptrs(mem_ptr + 4, obj_end, classes, new_object_table);
            }
            ObjectRef::Object(mem_ptr) => {
                let class_index = self.read_u16(mem_ptr);
                let number_of_fields = classes.get(&class_index).unwrap().fields.len();
                let obj_end = mem_ptr + 2 + (number_of_fields + 1) * PTR_SIZE;

                new_object_table
                    .insert(obj_ptr, ObjectRef::Object(self.free_cursor));

                self.copy_memory(mem_ptr, obj_end);

                self.copy_ptrs(mem_ptr + 2, obj_end, classes, new_object_table);
            }
        }
    }

    pub fn collect_garbage(&mut self, roots: Vec<HeapPointer>, classes: &Classes) {
        let mut new_object_table = ObjectTable::new();

        if self.free_cursor <= self.memory.len() / 2 {
            self.free_cursor = self.memory.len() / 2;
            self.free_mem_end = self.memory.len();
        } else {
            self.free_cursor = 0;
            self.free_mem_end = self.memory.len() / 2;
        };

        self.mem_start = self.free_cursor;

        for root_ptr in roots {
            self.copy_object(root_ptr, classes, &mut new_object_table);
        }

        self.object_table = new_object_table;

        self.log_gc();
    }

    fn size(&self) -> usize {
        self.free_cursor - self.mem_start
    }

    fn log_start(&mut self) {
        let size = self.size();

        if let Some(output) = &mut self.logging_output {
            output.write_str(
                format!(
                    "timestamp,event,heap\n{},S,{}\n",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos(),
                    size
                )
                .as_str(),
            );
        }
    }

    fn log_event(&mut self, event: char) {
        let size = self.size();

        if let Some(output) = &mut self.logging_output {
            output.write_str(
                format!(
                    "{},{},{}\n",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos(),
                    event,
                    size
                )
                .as_str(),
            )
        }
    }

    fn log_alloc(&mut self) {
        self.log_event('A');
    }

    fn log_gc(&mut self) {
        self.log_event('G');
    }
}

trait TaggedPointer {
    fn is_tagged(self) -> bool;
    fn read_tagged_value(self) -> RuntimeObject;
}

impl TaggedPointer for HeapPointer {
    fn is_tagged(self) -> bool {
        self & !PTR_PAYLOAD_MASK != 0
    }

    fn read_tagged_value(self) -> RuntimeObject {
        let tag = self & !PTR_PAYLOAD_MASK;
        let payload = self & PTR_PAYLOAD_MASK;

        match tag {
            NULL_TAG => RuntimeObject::Null,
            BOOLEAN_TAG => RuntimeObject::Boolean(payload > 0),
            INTEGER_TAG => RuntimeObject::Integer(payload as i32),
            _ => panic!("Unknown tagged value {}", self),
        }
    }
}

impl From<&RuntimeObject> for HeapPointer {
    fn from(object: &RuntimeObject) -> Self {
        match *object {
            RuntimeObject::Null => NULL_PTR,
            RuntimeObject::Boolean(value) => BOOLEAN_TAG | value as usize,
            RuntimeObject::Integer(value) => INTEGER_TAG | value as u32 as usize,
            _ => panic!("Cannot create tagged pointer from non primitive runtime value"),
        }
    }
}
