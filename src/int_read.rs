use std::{io::Read, panic};

pub trait IntRead {
    fn read_u8(&mut self) -> u8;
    fn read_u16(&mut self) -> u16;
    fn read_u32(&mut self) -> u32;
    fn read_i32(&mut self) -> i32;
}

impl<R: Read> IntRead for R {
    fn read_u8(&mut self) -> u8 {
        let mut buf: [u8; 1] = [0];

        match self.read_exact(&mut buf) {
            Ok(_) => u8::from_le_bytes(buf),
            Err(_) => panic!("Could not read u8 from the the reader"),
        }
    }

    fn read_u16(&mut self) -> u16 {
        let mut buf: [u8; 2] = [0, 0];

        match self.read_exact(&mut buf) {
            Ok(_) => u16::from_le_bytes(buf),
            Err(_) => panic!("Could not read u16 from the reader"),
        }
    }

    fn read_u32(&mut self) -> u32 {
        let mut buf: [u8; 4] = [0, 0, 0, 0];

        match self.read_exact(&mut buf) {
            Ok(_) => u32::from_le_bytes(buf),
            Err(_) => panic!("Could not read u32 from the reader"),
        }
    }

    fn read_i32(&mut self) -> i32 {
        let mut buf: [u8; 4] = [0, 0, 0, 0];

        match self.read_exact(&mut buf) {
            Ok(_) => i32::from_le_bytes(buf),
            Err(_) => panic!("Could not read i32 from the reader"),
        }
    }
}
