use std::{collections::HashMap, io::Read};

use crate::{
    instruction::{
        Instruction, ARRAY_OPCODE, BRANCH_OPCODE, CALL_FUNCTION_OPCODE, CALL_METHOD_OPCODE,
        DROP_OPCODE, GET_FIELD_OPCODE, GET_GLOBAL_OPCODE, GET_LOCAL_OPCODE, JUMP_OPCODE,
        LABEL_OPCODE, LITERAL_OPCODE, OBJECT_OPCODE, PRINT_OPCODE, RETURN_OPCODE, SET_FIELD_OPCODE,
        SET_GLOBAL_OPCODE, SET_LOCAL_OPCODE,
    },
    int_read::IntRead,
    program_object::{
        ProgramObject, BOOLEAN_TAG, CLASS_TAG, INTEGER_TAG, METHOD_TAG, NULL_TAG, SLOT_TAG,
        STRING_TAG,
    },
};

pub type InstructionPointer = usize;

pub type ConstantPoolIndex = u16;

pub type ConstantPool = Vec<ProgramObject>;

pub type Code = Vec<Instruction>;

pub type Globals = HashMap<ConstantPoolIndex, u16>;

pub type LabelTable = HashMap<ConstantPoolIndex, InstructionPointer>;

pub struct Class {
    pub methods: HashMap<ConstantPoolIndex, ConstantPoolIndex>,
    pub fields: HashMap<ConstantPoolIndex, u16>,
}

pub type Classes = HashMap<ConstantPoolIndex, Class>;

pub const HALT_ADDRESS: InstructionPointer = 0;

pub struct Program {
    pub code: Code,
    pub constant_pool: ConstantPool,
    pub global_vars: Globals,
    pub functions: Globals,
    pub entry_point_idx: ConstantPoolIndex,
    pub label_table: LabelTable,
    pub classes: Classes,
}

impl Program {
    fn read_instruction(&mut self, mut reader: &mut dyn Read) {
        match reader.read_u8() {
            LABEL_OPCODE => {
                self.label_table.insert(reader.read_u16(), self.code.len());
            }
            LITERAL_OPCODE => self.code.push(Instruction::Literal(reader.read_u16())),
            PRINT_OPCODE => self.code.push(Instruction::Print {
                format: reader.read_u16(),
                arguments: reader.read_u8(),
            }),
            ARRAY_OPCODE => self.code.push(Instruction::Array),
            OBJECT_OPCODE => self.code.push(Instruction::Object(reader.read_u16())),
            GET_FIELD_OPCODE => self.code.push(Instruction::GetField(reader.read_u16())),
            SET_FIELD_OPCODE => self.code.push(Instruction::SetField(reader.read_u16())),
            CALL_METHOD_OPCODE => self.code.push(Instruction::CallMethod {
                method_index: reader.read_u16(),
                arguments: reader.read_u8(),
            }),
            CALL_FUNCTION_OPCODE => self.code.push(Instruction::CallFunction {
                function_id: reader.read_u16(),
                arguments: reader.read_u8(),
            }),
            SET_LOCAL_OPCODE => self.code.push(Instruction::SetLocal(reader.read_u16())),
            GET_LOCAL_OPCODE => self.code.push(Instruction::GetLocal(reader.read_u16())),
            SET_GLOBAL_OPCODE => self.code.push(Instruction::SetGlobal(reader.read_u16())),
            GET_GLOBAL_OPCODE => self.code.push(Instruction::GetGlobal(reader.read_u16())),
            BRANCH_OPCODE => self.code.push(Instruction::Branch(reader.read_u16())),
            JUMP_OPCODE => self.code.push(Instruction::Jump(reader.read_u16())),
            RETURN_OPCODE => self.code.push(Instruction::Return),
            DROP_OPCODE => self.code.push(Instruction::Drop),
            opcode => panic!("Unknown opcode 0x{:02x}", opcode),
        };
    }

    fn read_method_code(&mut self, mut reader: &mut dyn Read) {
        let len = reader.read_u32();

        self.code.reserve(len as usize + 1);

        for _ in 0..len {
            self.read_instruction(reader);
        }

        self.code.push(Instruction::Return);
    }

    fn read_method(&mut self, mut reader: &mut dyn Read) -> ProgramObject {
        let method = ProgramObject::Method {
            name: reader.read_u16(),
            parameters: reader.read_u8(),
            locals: reader.read_u16(),
            start: self.code.len(),
        };

        self.read_method_code(reader);

        method
    }

    fn read_program_object(&mut self, mut reader: &mut dyn Read) {
        let tag = reader.read_u8();

        let object = match tag {
            INTEGER_TAG => ProgramObject::Integer(reader.read_i32()),
            NULL_TAG => ProgramObject::Null,
            STRING_TAG => ProgramObject::new_string(reader),
            METHOD_TAG => self.read_method(reader),
            SLOT_TAG => ProgramObject::Slot(reader.read_u16()),
            CLASS_TAG => self.read_class(reader),
            BOOLEAN_TAG => ProgramObject::Boolean(reader.read_u8() != 0x00),
            _ => panic!("Unknown program object tag 0x{:02x}", tag),
        };

        self.constant_pool.push(object)
    }

    fn read_constant_pool(&mut self, mut reader: &mut dyn Read) {
        let len = reader.read_u16();

        self.constant_pool.reserve_exact(len as usize);

        for _ in 0..len {
            self.read_program_object(reader)
        }
    }

    fn read_globals(&mut self, mut reader: &mut dyn Read) {
        let len = reader.read_u16();

        for _ in 0..len {
            let constant_pool_idx = reader.read_u16();
            let program_object: &ProgramObject = self
                .constant_pool
                .get(constant_pool_idx as usize)
                .unwrap_or_else(|| {
                    panic!(
                        "Global object with constant pool index {} not found",
                        constant_pool_idx
                    )
                });

            match *program_object {
                ProgramObject::Slot(name) => {
                    self.global_vars.insert(name, self.global_vars.len() as u16);
                }
                ProgramObject::Method { name, .. } => {
                    self.functions.insert(name, constant_pool_idx);
                }
                _ => panic!("Invalid global program object type"),
            }
        }
    }

    fn read_class(&mut self, mut reader: &mut dyn Read) -> ProgramObject {
        let length = reader.read_u16();
        let members: Vec<ConstantPoolIndex> = (0..length).map(|_| reader.read_u16()).collect();

        ProgramObject::Class(members)
    }

    fn process_classes(&mut self) {
        let classes_iter = self.constant_pool
            .iter()
            .enumerate()
            .filter_map(|(const_pool_idx, program_object)| {
                match program_object {
                    ProgramObject::Class(members) => {
                        let mut class = Class {
                            methods: HashMap::new(),
                            fields: HashMap::new(),
                        };

                        for member_idx in members {
                            let member_object = self.constant_pool.get(*member_idx as usize).unwrap_or_else(|| {
                                panic!(
                                    "Class member program object on constant pool index {} not found",
                                    member_idx
                                )
                            });

                            match *member_object {
                                ProgramObject::Slot(name_idx) => class.fields.insert(name_idx, class.fields.len() as u16),
                                ProgramObject::Method { name, .. } => class.methods.insert(name, *member_idx),
                                _ => panic!("Class member program object must be either a slot or a method")
                            };
                        }

                        Some((const_pool_idx as u16, class))
                    }
                    _ => None,
                }
            });

        for (const_pool_idx, class) in classes_iter {
            self.classes.insert(const_pool_idx, class);
        }
    }

    fn load(&mut self, mut reader: &mut dyn Read) {
        self.read_constant_pool(reader);
        self.process_classes();
        self.read_globals(reader);
        self.entry_point_idx = reader.read_u16();
    }

    pub fn get_class(&self, class_idx: ConstantPoolIndex) -> &Class {
        self.classes
            .get(&class_idx)
            .unwrap_or_else(|| panic!("Class with constant pool index {} not found", class_idx))
    }

    pub fn get_string(&self, idx: ConstantPoolIndex) -> &String {
        let object = self
            .constant_pool
            .get(idx as usize)
            .unwrap_or_else(|| panic!("String program object on index {} not found", idx));

        match object {
            ProgramObject::String(string) => string,
            _ => panic!(
                "Program object on constant pool index {} is not a string",
                idx
            ),
        }
    }
}

impl From<&mut dyn Read> for Program {
    fn from(reader: &mut dyn Read) -> Self {
        let mut program = Program {
            constant_pool: ConstantPool::default(),
            code: vec![Instruction::Halt],
            global_vars: Globals::new(),
            entry_point_idx: 0,
            label_table: LabelTable::new(),
            functions: Globals::new(),
            classes: Classes::new(),
        };

        program.load(reader);

        program
    }
}
