use std::slice::Iter;

pub struct Stack<T> {
    vec: Vec<T>,
}

impl<T> Stack<T> {
    pub fn new(capacity: usize) -> Self {
        Stack {
            vec: Vec::with_capacity(capacity),
        }
    }

    pub fn push(&mut self, value: T) {
        if self.vec.len() == self.vec.capacity() {
            panic!("Stack overflow")
        }

        self.vec.push(value)
    }

    pub fn pop(&mut self) -> T {
        self.vec
            .pop()
            .unwrap_or_else(|| panic!("Can't pop out of empty stack"))
    }

    pub fn peek(&self) -> &T {
        self.vec
            .last()
            .unwrap_or_else(|| panic!("Can't peek on empty stack"))
    }

    pub fn peek_mut(&mut self) -> &mut T {
        self.vec
            .last_mut()
            .unwrap_or_else(|| panic!("Can't peek on empty stack"))
    }

    pub fn iter(&self) -> Iter<'_, T> {
        self.vec.iter()
    }
}
