use std::{
    fs::File,
    io::{stderr, stdout, Read}
};

use crate::interpreter::Interpreter;

fn run_file(filename: &str) {
    let reader: &mut dyn Read = &mut File::open(filename).unwrap();
    Interpreter::new(reader.into(), 520, stdout(), Some(stderr())).run();
}

#[test]
fn should_print() {
    run_file("test_files/print.bc")
}

#[test]
fn should_correctly_scope() {
    run_file("test_files/scoping.bc")
}

#[test]
fn should_correctly_work_with_locals() {
    run_file("test_files/locals.bc")
}

#[test]
fn should_print_true_true() {
    run_file("test_files/function2.bc")
}

#[test]
fn debug() {
    run_file("test.bc")
}
