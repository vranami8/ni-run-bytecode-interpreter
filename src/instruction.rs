use crate::program::ConstantPoolIndex;

pub const LABEL_OPCODE: u8 = 0x00;
pub const LITERAL_OPCODE: u8 = 0x01;
pub const PRINT_OPCODE: u8 = 0x02;
pub const ARRAY_OPCODE: u8 = 0x03;
pub const OBJECT_OPCODE: u8 = 0x04;
pub const GET_FIELD_OPCODE: u8 = 0x05;
pub const SET_FIELD_OPCODE: u8 = 0x06;
pub const CALL_METHOD_OPCODE: u8 = 0x07;
pub const CALL_FUNCTION_OPCODE: u8 = 0x08;
pub const SET_LOCAL_OPCODE: u8 = 0x09;
pub const GET_LOCAL_OPCODE: u8 = 0x0A;
pub const SET_GLOBAL_OPCODE: u8 = 0x0B;
pub const GET_GLOBAL_OPCODE: u8 = 0x0C;
pub const BRANCH_OPCODE: u8 = 0x0D;
pub const JUMP_OPCODE: u8 = 0x0E;
pub const RETURN_OPCODE: u8 = 0x0F;
pub const DROP_OPCODE: u8 = 0x10;

pub enum Instruction {
    Literal(ConstantPoolIndex),
    GetLocal(u16),
    SetLocal(u16),
    GetGlobal(ConstantPoolIndex),
    SetGlobal(ConstantPoolIndex),
    CallFunction {
        function_id: ConstantPoolIndex,
        arguments: u8,
    },
    Return,
    Jump(ConstantPoolIndex),
    Branch(ConstantPoolIndex),
    Print {
        format: ConstantPoolIndex,
        arguments: u8,
    },
    Array,
    Object(ConstantPoolIndex),
    GetField(ConstantPoolIndex),
    SetField(ConstantPoolIndex),
    CallMethod {
        method_index: ConstantPoolIndex,
        arguments: u8,
    },
    Drop,
    Halt
}
