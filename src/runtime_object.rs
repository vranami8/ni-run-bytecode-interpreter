use crate::{heap::HeapPointer, program::ConstantPoolIndex, program_object::ProgramObject};

pub enum RuntimeObject {
    Null,
    Integer(i32),
    Boolean(bool),
    Array {
        size: u32,
        content: HeapPointer,
    },
    Object {
        class: ConstantPoolIndex,
        parent: HeapPointer,
        content: HeapPointer,
    },
}

impl RuntimeObject {
    pub fn get_int_value(&self) -> i32 {
        match self {
            RuntimeObject::Integer(value) => *value,
            _ => panic!("Cannot get an integer value out of a non integer object"),
        }
    }

    pub fn get_bool_value(&self) -> bool {
        match self {
            RuntimeObject::Boolean(value) => *value,
            _ => panic!("Cannot get a boolean value out of a non boolean object"),
        }
    }


    fn is_equal(&self, other: &RuntimeObject) -> bool {
        match *self {
            RuntimeObject::Null => match *other {
                RuntimeObject::Null => true,
                _ => false,
            },
            RuntimeObject::Integer(left) => match *other {
                RuntimeObject::Integer(right) => left == right,
                _ => false,
            },
            RuntimeObject::Boolean(left) => match *other {
                RuntimeObject::Boolean(right) => left == right,
                _ => false,
            },
            _ => panic!("Cannot compare complex types"),
        }
    }

    pub fn equal(&self, other: &RuntimeObject) -> RuntimeObject {
        RuntimeObject::Boolean(self.is_equal(other))
    }

    pub fn not_equal(&self, other: &RuntimeObject) -> RuntimeObject {
        RuntimeObject::Boolean(!self.is_equal(other))
    }

    pub fn add(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Integer(left + right)
    }

    pub fn subtract(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Integer(left - right)
    }

    pub fn multiply(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Integer(left * right)
    }

    pub fn divide(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        if right == 0 {
            panic!("Cannot divide by zero")
        }

        RuntimeObject::Integer(left / right)
    }

    pub fn modulo(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        if right == 0 {
            panic!("Cannot modulo by zero")
        }

        RuntimeObject::Integer(left % right)
    }

    pub fn less_or_equal(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Boolean(left <= right)
    }

    pub fn greater_or_equal(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Boolean(left >= right)
    }

    pub fn less(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Boolean(left < right)
    }

    pub fn greater(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_int_value();
        let right = other.get_int_value();

        RuntimeObject::Boolean(left > right)
    }

    pub fn and(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_bool_value();
        let right = other.get_bool_value();

        RuntimeObject::Boolean(left && right)
    }

    pub fn or(&self, other: &RuntimeObject) -> RuntimeObject {
        let left = self.get_bool_value();
        let right = other.get_bool_value();

        RuntimeObject::Boolean(left || right)
    }
}

impl From<&ProgramObject> for RuntimeObject {
    fn from(program_object: &ProgramObject) -> Self {
        match *program_object {
            ProgramObject::Null => RuntimeObject::Null,
            ProgramObject::Integer(value) => RuntimeObject::Integer(value),
            ProgramObject::Boolean(value) => RuntimeObject::Boolean(value),
            ProgramObject::String(_) => panic!("Cannot convert string to runtime object"),
            ProgramObject::Method { .. } => panic!("Cannot convert method to runtime object"),
            ProgramObject::Slot(_) => panic!("Cannot convert slot to runtime object"),
            ProgramObject::Class { .. } => panic!("Cannot convert class to runtime object"),
        }
    }
}
