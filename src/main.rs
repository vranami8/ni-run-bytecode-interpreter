use interpreter::Interpreter;
use program::Program;
use std::{
    env::args,
    io::{stderr, stdin, stdout, Read},
};

#[cfg(test)]
mod tests;

mod heap;
mod helpers;
mod instruction;
mod int_read;
mod interpreter;
mod program;
mod program_object;
mod runtime_object;
mod stack;
mod stack_frame;
mod string_write;

fn main() {
    let args = &mut args();

    let heap_size = args.skip(1).next().unwrap().parse().unwrap();

    let heap_log_output = if args.next().unwrap_or_else(|| String::default()) == "--log-heap" {
        Some(stderr())
    } else {
        None
    };

    let stdin: &mut dyn Read = &mut stdin();
    let program = Program::from(stdin);
    Interpreter::new(program, heap_size, stdout(), heap_log_output).run()
}
