use std::io::Write;

use crate::{
    heap::{Heap, HeapPointer, NULL_PTR, PTR_SIZE},
    helpers::PanicOnErr,
    instruction::Instruction,
    program::{ConstantPoolIndex, InstructionPointer, Program, HALT_ADDRESS},
    program_object::ProgramObject,
    runtime_object::RuntimeObject,
    stack::Stack,
    stack_frame::StackFrame,
    string_write::StringWrite,
};

const OPERAND_STACK_SIZE: usize = 1024;
const FRAME_STACK_SIZE: usize = 1 << 7;

pub struct Interpreter<W1: Write, W2: Write> {
    program: Program,
    operand_stack: Stack<HeapPointer>,
    heap: Heap<W2>,
    ip: InstructionPointer,
    global_vars: Vec<HeapPointer>,
    frame_stack: Stack<StackFrame>,
    output: W1,
}

impl<W1: Write, W2: Write> Interpreter<W1, W2> {
    pub fn new(
        program: Program,
        heap_size: usize,
        output: W1,
        heap_logging_output: Option<W2>,
    ) -> Self {
        Interpreter {
            program,
            operand_stack: Stack::new(OPERAND_STACK_SIZE),
            heap: Heap::new(heap_size, heap_logging_output),
            ip: 0,
            frame_stack: Stack::new(FRAME_STACK_SIZE),
            global_vars: Vec::new(),
            output,
        }
    }

    fn pop_operand_and_read(&mut self) -> RuntimeObject {
        self.heap.read_object(self.operand_stack.pop())
    }

    fn push_literal(&mut self, index: ConstantPoolIndex) {
        let object = self
            .program
            .constant_pool
            .get(index as usize)
            .unwrap_or_else(|| panic!("Invalid constant pool index {}", index));

        let ptr = self.heap.alloc_primitve(&RuntimeObject::from(object));
        self.operand_stack.push(ptr);
        self.bump_ip();
    }

    fn print_runtime_object(&self, output_string: &mut String, object: &RuntimeObject) {
        match *object {
            RuntimeObject::Null => output_string.push_str("null"),
            RuntimeObject::Integer(value) => output_string.push_str(value.to_string().as_str()),
            RuntimeObject::Boolean(value) => output_string.push_str(value.to_string().as_str()),
            RuntimeObject::Array { size, content } => {
                output_string.push('[');

                for idx in 0..size {
                    let elem_value_ptr = self.heap.read_ptr(content + idx as usize * PTR_SIZE);
                    let elem_value = self.heap.read_object(elem_value_ptr);
                    self.print_runtime_object(output_string, &elem_value);

                    output_string.push_str(", ");
                }

                if size > 0 {
                    // remove the ending ", "
                    output_string.pop();
                    output_string.pop();
                }

                output_string.push(']');
            }
            RuntimeObject::Object {
                class: class_idx,
                parent,
                content,
            } => {
                output_string.push_str("object(");

                if parent != NULL_PTR {
                    output_string.push_str("..=");
                    let parent_object = &self.heap.read_object(parent);
                    self.print_runtime_object(output_string, parent_object);
                    output_string.push_str(", ");
                }

                let class = self.program.get_class(class_idx);

                let mut fields: Vec<(&String, u16)> = class
                    .fields
                    .iter()
                    .map(|(key, value)| (self.program.get_string(*key), *value))
                    .collect();

                fields.sort_by_key(|pair| pair.0);

                for (field_name, field_index) in fields.iter() {
                    output_string.push_str(field_name.as_str());
                    output_string.push('=');

                    let field_value_ptr = self
                        .heap
                        .read_ptr(content + *field_index as usize * PTR_SIZE);

                    let field_value = self.heap.read_object(field_value_ptr);
                    self.print_runtime_object(output_string, &field_value);

                    output_string.push_str(", ");
                }

                if fields.len() > 0 || parent != NULL_PTR {
                    // remove the ending ", "
                    output_string.pop();
                    output_string.pop();
                }

                output_string.push(')');
            }
        }
    }

    fn print(&mut self, format_idx: ConstantPoolIndex, arguments: u8) {
        let format_object = self
            .program
            .constant_pool
            .get(format_idx as usize)
            .unwrap_or_else(|| panic!("Invalid constant pool index {}", format_idx));

        let format = match format_object {
            // clone is unfortunately needed to satisfy borrow checker
            ProgramObject::String(format) => format,
            _ => panic!("Print format program object is not a string"),
        };

        let mut arguments_vec: Vec<RuntimeObject> = (0..arguments)
            .map(|_| self.heap.read_object(self.operand_stack.pop()))
            .collect();

        let mut is_escaped = false;

        let mut output_string = String::with_capacity(format.len() * 2);

        for char in format.chars() {
            if is_escaped {
                match char {
                    'n' => output_string.push('\n'),
                    'r' => output_string.push('\r'),
                    't' => output_string.push('\t'),
                    '\\' => output_string.push('\\'),
                    '~' => output_string.push('~'),
                    '"' => output_string.push('"'),
                    _ => panic!("Invalid escaped character {}", char),
                };

                is_escaped = false;
                continue;
            }

            match char {
                '\\' => is_escaped = true,
                '~' => self.print_runtime_object(&mut output_string, &arguments_vec.pop().unwrap()),
                _ => output_string.push(char),
            }
        }

        self.output.write_str(output_string.as_str());
        self.output.flush().panic_on_err();
        self.operand_stack.push(NULL_PTR);
        self.bump_ip();
    }

    fn func_call_return(&mut self) {
        self.ip = self.frame_stack.pop().return_address;
    }

    fn get_local(&mut self, local_idx: u16) {
        let frame = self.frame_stack.peek();
        let ptr = frame
            .vars
            .get(local_idx as usize)
            .unwrap_or_else(|| panic!("Local variable with index {} not found", local_idx));

        self.operand_stack.push(*ptr);
        self.bump_ip();
    }

    fn set_local(&mut self, local_idx: u16) {
        let frame = self.frame_stack.peek_mut();
        let var_ref = frame
            .vars
            .get_mut(local_idx as usize)
            .unwrap_or_else(|| panic!("Local variable with index {} not found", local_idx));

        *var_ref = *self.operand_stack.peek();
        self.bump_ip()
    }

    fn get_global(&mut self, global_id: ConstantPoolIndex) {
        let global_idx = self
            .program
            .global_vars
            .get(&global_id)
            .unwrap_or_else(|| panic!("Global variable with ID {} not found", global_id));

        let ptr = self
            .global_vars
            .get(*global_idx as usize)
            .unwrap_or_else(|| panic!("Global variable with index {} not found", global_idx));

        self.operand_stack.push(*ptr);
        self.bump_ip()
    }

    fn set_global(&mut self, global_id: ConstantPoolIndex) {
        let global_idx = self
            .program
            .global_vars
            .get(&global_id)
            .unwrap_or_else(|| panic!("Global variable with ID {} not found", global_id));

        let var_ref = self
            .global_vars
            .get_mut(*global_idx as usize)
            .unwrap_or_else(|| panic!("Global variable with index {} not found", global_idx));

        *var_ref = *self.operand_stack.peek();
        self.bump_ip()
    }

    fn jump(&mut self, label_id: u16) {
        self.ip = *self
            .program
            .label_table
            .get(&label_id)
            .unwrap_or_else(|| panic!("Label with id {} not found", label_id));
    }

    fn branch(&mut self, label_id: u16) {
        match self.pop_operand_and_read() {
            RuntimeObject::Null => self.bump_ip(),
            RuntimeObject::Boolean(false) => self.bump_ip(),
            _ => self.jump(label_id),
        }
    }

    fn call_function(
        &mut self,
        function_index: ConstantPoolIndex,
        argument_ptrs: Vec<HeapPointer>,
        return_address: InstructionPointer,
    ) {
        let program_object = self
            .program
            .constant_pool
            .get(function_index as usize)
            .unwrap_or_else(|| {
                panic!(
                    "Method object with constant pool index {} not found",
                    function_index
                )
            });

        match program_object {
            ProgramObject::Method {
                parameters,
                locals,
                start,
                ..
            } => {
                if *parameters as usize != argument_ptrs.len() {
                    panic!(
                        "Invalid number of arguments, provided {}, expected {}",
                        argument_ptrs.len(),
                        parameters
                    )
                }

                let locals = (0..*locals).map(|_| NULL_PTR);

                self.frame_stack.push(StackFrame {
                    vars: argument_ptrs.into_iter().rev().chain(locals).collect(),
                    return_address,
                });

                self.ip = *start;
            }
            _ => panic!("Invalid program object, expected method"),
        }
    }

    fn exec_call_function(&mut self, function_id: ConstantPoolIndex, arguments: u8) {
        let constant_pool_idx = *self
            .program
            .functions
            .get(&function_id)
            .unwrap_or_else(|| panic!("Function with ID {} not found", function_id));

        let argument_ptrs = (0..arguments).map(|_| self.operand_stack.pop()).collect();

        self.call_function(constant_pool_idx, argument_ptrs, self.ip + 1);
    }

    fn drop(&mut self) {
        self.operand_stack.pop();
        self.bump_ip();
    }

    fn push_array(&mut self) {
        let initial_value_ptr = self.operand_stack.pop();
        let size_object = self.pop_operand_and_read();

        match size_object {
            RuntimeObject::Integer(size) => {
                if size < 0 {
                    panic!("Array size must be greater or equal to zero");
                }

                if !self.heap.has_space(size as usize * PTR_SIZE + 4) {
                    let mut roots = self.get_roots();
                    roots.push(initial_value_ptr);

                    self.heap.collect_garbage(roots, &self.program.classes);
                }

                let array_ptr = self.heap.alloc_array(size as u32, initial_value_ptr);
                self.operand_stack.push(array_ptr);
            }
            _ => panic!("Array size value must be an integer"),
        }

        self.bump_ip();
    }

    fn push_object(&mut self, class_idx: ConstantPoolIndex) {
        let class = self.program.get_class(class_idx);

        if !self.heap.has_space((class.fields.len() + 1) * PTR_SIZE + 2) {
            self.heap
                .collect_garbage(self.get_roots(), &self.program.classes);
        }

        let field_value_ptrs: Vec<HeapPointer> = (0..class.fields.len())
            .map(|_| self.operand_stack.pop())
            .collect();

        let parent_ptr = self.operand_stack.pop();

        let object_ptr = self.heap.alloc_object(
            class_idx,
            parent_ptr,
            field_value_ptrs.into_iter().rev().collect(),
        );

        self.operand_stack.push(object_ptr);
        self.bump_ip();
    }

    // returns a pointer to field, so it essentialy returns a pointer to another pointer
    fn get_field_ptr(
        &mut self,
        field_name_idx: ConstantPoolIndex,
        object_ptr: HeapPointer,
    ) -> HeapPointer {
        let object = self.heap.read_object(object_ptr);

        match object {
            RuntimeObject::Object {
                class: class_idx,
                content,
                ..
            } => {
                let class = self.program.get_class(class_idx);

                let field_idx = class.fields.get(&field_name_idx).unwrap_or_else(|| {
                    panic!("Field with name index {} not found", field_name_idx)
                });

                content + *field_idx as usize * PTR_SIZE
            }
            _ => panic!("Cannot access a field of a non object value"),
        }
    }

    fn get_field(&mut self, field_name_idx: ConstantPoolIndex) {
        let object_ptr = self.operand_stack.pop();
        let field_ptr = self.get_field_ptr(field_name_idx, object_ptr);
        let value_ptr = self.heap.read_ptr(field_ptr);

        self.operand_stack.push(value_ptr);
        self.bump_ip();
    }

    fn set_field(&mut self, field_name_idx: ConstantPoolIndex) {
        let value_ptr = self.operand_stack.pop();
        let object_ptr = self.operand_stack.pop();
        let field_ptr = self.get_field_ptr(field_name_idx, object_ptr);

        self.heap.write_ptr(field_ptr, value_ptr);

        self.operand_stack.push(value_ptr);
        self.bump_ip();
    }

    fn get_constant_pool_string(&self, string_idx: ConstantPoolIndex) -> &String {
        let object = self
            .program
            .constant_pool
            .get(string_idx as usize)
            .unwrap_or_else(|| {
                panic!(
                    "Method name with constant pool index {} not founds",
                    string_idx
                )
            });

        match object {
            ProgramObject::String(string) => string,
            _ => panic!("Invalid program object type, expected string"),
        }
    }

    fn call_array_method(
        &mut self,
        method_index: u16,
        argument_ptrs: Vec<HeapPointer>,
        size: u32,
        content: usize,
    ) {
        let method_name = self.get_constant_pool_string(method_index);

        match method_name.as_str() {
            "get" => {
                if argument_ptrs.len() != 1 {
                    panic!(
                        "Invalid number of method arguments, provided {}, expected 2",
                        argument_ptrs.len()
                    )
                }

                let index_ptr = *argument_ptrs
                    .first()
                    .unwrap_or_else(|| panic!("Missing argument"));

                let index = self.heap.read_object(index_ptr).get_int_value();

                if index as u32 >= size || index < 0 {
                    panic!("Array out of bounds access at index {}", index)
                }

                let value_ptr = self.heap.read_ptr(content + index as usize * PTR_SIZE);
                self.operand_stack.push(value_ptr);
            }
            "set" => {
                if argument_ptrs.len() != 2 {
                    panic!(
                        "Invalid number of method arguments, provided {}, expected 3",
                        argument_ptrs.len()
                    )
                }

                let value_ptr = *argument_ptrs
                    .first()
                    .unwrap_or_else(|| panic!("Missing argument"));

                let index_ptr = *argument_ptrs
                    .get(1)
                    .unwrap_or_else(|| panic!("Missing argument"));

                let index = self.heap.read_object(index_ptr).get_int_value();

                if index as u32 >= size || index < 0 {
                    panic!("Array out of bounds access at index {}", index)
                }

                self.heap
                    .write_ptr(content + index as usize * PTR_SIZE, value_ptr);
                self.operand_stack.push(value_ptr);
            }
            _ => panic!("Unknown method {}", method_name),
        }

        self.bump_ip()
    }

    fn call_method(
        &mut self,
        receiver_ptr: HeapPointer,
        method_index: ConstantPoolIndex,
        mut argument_ptrs: Vec<HeapPointer>,
    ) {
        let receiver = self.heap.read_object(receiver_ptr);

        match receiver {
            RuntimeObject::Object {
                class: class_idx,
                parent,
                ..
            } => {
                let class = self.program.get_class(class_idx);
                let method_object_idx = class.methods.get(&method_index);

                match method_object_idx {
                    Some(&method_object_idx) => {
                        argument_ptrs.push(receiver_ptr);
                        self.call_function(method_object_idx, argument_ptrs, self.ip + 1);
                    }
                    None => self.call_method(parent, method_index, argument_ptrs),
                }
            }
            RuntimeObject::Array { size, content } => {
                self.call_array_method(method_index, argument_ptrs, size, content)
            }
            _ => {
                if argument_ptrs.len() != 1 {
                    panic!(
                        "Invalid number of method arguments, provided {}, expected 2",
                        argument_ptrs.len()
                    )
                }

                let argument_ptr = *argument_ptrs
                    .first()
                    .unwrap_or_else(|| panic!("Missing argument"));

                let argument = &self.heap.read_object(argument_ptr);
                let method_name = self.get_constant_pool_string(method_index);

                let result = match method_name.as_str() {
                    "==" => receiver.equal(argument),
                    "!=" => receiver.not_equal(argument),
                    "+" => receiver.add(argument),
                    "-" => receiver.subtract(argument),
                    "*" => receiver.multiply(argument),
                    "/" => receiver.divide(argument),
                    "%" => receiver.modulo(argument),
                    "<=" => receiver.less_or_equal(argument),
                    ">=" => receiver.greater_or_equal(argument),
                    "<" => receiver.less(argument),
                    ">" => receiver.greater(argument),
                    "&" => receiver.and(argument),
                    "|" => receiver.or(argument),
                    "eq" => receiver.equal(argument),
                    "neq" => receiver.not_equal(argument),
                    "add" => receiver.add(argument),
                    "sub" => receiver.subtract(argument),
                    "mul" => receiver.multiply(argument),
                    "div" => receiver.divide(argument),
                    "mod" => receiver.modulo(argument),
                    "le" => receiver.less_or_equal(argument),
                    "ge" => receiver.greater_or_equal(argument),
                    "lt" => receiver.less(argument),
                    "gt" => receiver.greater(argument),
                    "and" => receiver.and(argument),
                    "or" => receiver.or(argument),
                    _ => panic!("Unknown method '{}'", method_name),
                };

                let result_ptr = self.heap.alloc_primitve(&result);
                self.operand_stack.push(result_ptr);
                self.bump_ip();
            }
        }
    }

    fn exec_call_method(&mut self, method_index: ConstantPoolIndex, arguments: u8) {
        let argument_ptrs: Vec<HeapPointer> = (0..arguments - 1)
            .map(|_| self.operand_stack.pop())
            .collect();

        let receiver_ptr = self.operand_stack.pop();
        self.call_method(receiver_ptr, method_index, argument_ptrs);
    }

    fn bump_ip(&mut self) {
        self.ip = self.ip + 1
    }

    fn execute_instructions(mut self) {
        loop {
            let instruction = self
                .program
                .code
                .get(self.ip)
                .unwrap_or_else(|| panic!("Instruction fetch failed"));

            match *instruction {
                Instruction::Literal(index) => self.push_literal(index),
                Instruction::GetLocal(local_idx) => self.get_local(local_idx),
                Instruction::SetLocal(local_idx) => self.set_local(local_idx),
                Instruction::GetGlobal(global_id) => self.get_global(global_id),
                Instruction::SetGlobal(global_id) => self.set_global(global_id),
                Instruction::CallFunction {
                    function_id,
                    arguments,
                } => self.exec_call_function(function_id, arguments),
                Instruction::Return => self.func_call_return(),
                Instruction::Jump(label_id) => self.jump(label_id),
                Instruction::Branch(label_id) => self.branch(label_id),
                Instruction::Print { format, arguments } => {
                    self.print(format, arguments);
                }
                Instruction::Array => self.push_array(),
                Instruction::Object(class_idx) => self.push_object(class_idx),
                Instruction::GetField(field_name_idx) => self.get_field(field_name_idx),
                Instruction::SetField(field_name_idx) => self.set_field(field_name_idx),
                Instruction::CallMethod {
                    method_index,
                    arguments,
                } => self.exec_call_method(method_index, arguments),
                Instruction::Drop => self.drop(),
                Instruction::Halt => return,
            }
        }
    }

    fn get_roots(&self) -> Vec<HeapPointer> {
        let mut frame_stack_roots: Vec<HeapPointer> = self
            .frame_stack
            .iter()
            .flat_map(|frame| frame.vars.clone())
            .collect();
        frame_stack_roots.extend(self.operand_stack.iter());
        frame_stack_roots.extend(self.global_vars.iter());
        frame_stack_roots
    }

    pub fn run(mut self) {
        self.global_vars
            .resize(self.program.global_vars.len(), NULL_PTR);
        self.call_function(self.program.entry_point_idx, Vec::default(), HALT_ADDRESS);
        self.execute_instructions();
    }
}
