use std::io;

pub trait PanicOnErr {
    fn panic_on_err(&self);
}

impl<T> PanicOnErr for io::Result<T> {
    fn panic_on_err(&self) {
        match self {
            Ok(_) => (),
            Err(err) => panic!("IO error: {}", err),
        }
    }
}
