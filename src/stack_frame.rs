use crate::{heap::HeapPointer, program::InstructionPointer};

pub struct StackFrame {
  pub vars: Vec<HeapPointer>,
  pub return_address: InstructionPointer
}