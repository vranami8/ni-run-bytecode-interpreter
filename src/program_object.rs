use std::io::Read;

use crate::{
    int_read::IntRead,
    program::{ConstantPoolIndex, InstructionPointer},
};

pub const INTEGER_TAG: u8 = 0x00;
pub const NULL_TAG: u8 = 0x01;
pub const STRING_TAG: u8 = 0x02;
pub const METHOD_TAG: u8 = 0x03;
pub const SLOT_TAG: u8 = 0x04;
pub const CLASS_TAG: u8 = 0x05;
pub const BOOLEAN_TAG: u8 = 0x06;

pub enum ProgramObject {
    Null,
    Integer(i32),
    Boolean(bool),
    String(String),
    Slot(ConstantPoolIndex),
    Method {
        name: ConstantPoolIndex,
        parameters: u8,
        locals: u16,
        start: InstructionPointer,
    },
    Class(Vec<ConstantPoolIndex>),
}

impl ProgramObject {
    pub fn new_string(mut reader: &mut dyn Read) -> Self {
        let len = reader.read_u32();
        let mut str = String::with_capacity(len as usize);

        for _ in 0..len {
            str.push(reader.read_u8().into());
        }

        ProgramObject::String(str)
    }
}
