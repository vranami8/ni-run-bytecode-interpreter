use std::io::Write;

use crate::helpers::PanicOnErr;

pub trait StringWrite {
    fn write_str(&mut self, str: &str);
}

impl<W: Write> StringWrite for W {
    fn write_str(&mut self, str: &str) {
        self.write(str.as_bytes()).panic_on_err()
    }
}
